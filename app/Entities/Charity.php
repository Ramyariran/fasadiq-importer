<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Charity.
 */
class Charity extends Model
{
  protected $table = 'charities';

  protected $guarded = ['id'];

  protected $fillable = [
    'num',
    'category',
    'no',
    'organization',
    'amount',
    'province',
    'city',
    'twitter',
    'website',
    'text'
  ];
}
