<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use MrShan0\PHPFirestore\FirestoreClient;
use MrShan0\PHPFirestore\FirestoreDocument;

use App\Imports\CharitiesImport;

use App\Entities\Charity;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
      return 'some text';
    }

    public function import()
    {
      $charities = Excel::import(new CharitiesImport, 'charities.xlsx');

      return 'done';
    }

    public function upload()
    {
      dd(1);
      $charities = Charity::skip(200)->limit(50)
        ->where('category', '<>', '')
        ->where('category', 'NOT LIKE', '%مسلمين%')
        ->get();
     
      $firestoreClient = new FirestoreClient('fasadiqq', 'AIzaSyCem05agMUzqjLHC6F8vADV291g8abZ_lA', [
        'database' => '(default)',
      ]);

      foreach ($charities as $charity) {
        $document = new FirestoreDocument;
        
        $document->setString('id', strval($charity->id));
        $document->setString('num', strval($charity->unm));
        $document->setString('category', strval($charity->category));
        $document->setString('no', strval($charity->no));
        $document->setString('organization', strval($charity->organization));
        $document->setString('amount', strval($charity->amount));
        $document->setString('province', strval($charity->province));
        $document->setString('city', strval($charity->city));
        $document->setString('twitter', strval($charity->twitter));
        $document->setString('website', strval($charity->website));
        $document->setString('text', strval($charity->text));

        $firestoreClient->addDocument('charities', $document);
      }

      return $charities->count();
    }
}

