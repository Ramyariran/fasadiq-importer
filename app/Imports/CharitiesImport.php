<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;

use App\Entities\Charity;

class CharitiesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      if ($row[0] == 'num' || strval($row[3]) == '') return;

      return new Charity([
        'num' => strval($row[0]),
        'category' => strval($row[1]),
        'no' => strval($row[2]),
        'organization' => strval($row[3]),
        'amount' => strval($row[5]),
        'province' => strval($row[6]),
        'city' => strval($row[7]),
        'twitter' => strval($row[8]),
        'website' => strval($row[9]),
        'text' => strval($row[10])
      ]);
    }
}