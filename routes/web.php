<?php

Auth::routes();

/******************* Home *******************/

Route::name('home')->get('/', 'HomeController@home');
Route::name('import')->get('/import', 'HomeController@import');
Route::name('upload')->get('/upload', 'HomeController@upload');

Route::get('/el-admin', 'BackendController@index');
